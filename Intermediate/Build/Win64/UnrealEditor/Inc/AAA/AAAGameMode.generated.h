// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef AAA_AAAGameMode_generated_h
#error "AAAGameMode.generated.h already included, missing '#pragma once' in AAAGameMode.h"
#endif
#define AAA_AAAGameMode_generated_h

#define FID_A3_P1_Source_AAA_AAAGameMode_h_12_SPARSE_DATA
#define FID_A3_P1_Source_AAA_AAAGameMode_h_12_RPC_WRAPPERS
#define FID_A3_P1_Source_AAA_AAAGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_A3_P1_Source_AAA_AAAGameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAAAAGameMode(); \
	friend struct Z_Construct_UClass_AAAAGameMode_Statics; \
public: \
	DECLARE_CLASS(AAAAGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/AAA"), AAA_API) \
	DECLARE_SERIALIZER(AAAAGameMode)


#define FID_A3_P1_Source_AAA_AAAGameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAAAAGameMode(); \
	friend struct Z_Construct_UClass_AAAAGameMode_Statics; \
public: \
	DECLARE_CLASS(AAAAGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/AAA"), AAA_API) \
	DECLARE_SERIALIZER(AAAAGameMode)


#define FID_A3_P1_Source_AAA_AAAGameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	AAA_API AAAAGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AAAAGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(AAA_API, AAAAGameMode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAAAGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	AAA_API AAAAGameMode(AAAAGameMode&&); \
	AAA_API AAAAGameMode(const AAAAGameMode&); \
public:


#define FID_A3_P1_Source_AAA_AAAGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	AAA_API AAAAGameMode(AAAAGameMode&&); \
	AAA_API AAAAGameMode(const AAAAGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(AAA_API, AAAAGameMode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAAAGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AAAAGameMode)


#define FID_A3_P1_Source_AAA_AAAGameMode_h_9_PROLOG
#define FID_A3_P1_Source_AAA_AAAGameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_A3_P1_Source_AAA_AAAGameMode_h_12_SPARSE_DATA \
	FID_A3_P1_Source_AAA_AAAGameMode_h_12_RPC_WRAPPERS \
	FID_A3_P1_Source_AAA_AAAGameMode_h_12_INCLASS \
	FID_A3_P1_Source_AAA_AAAGameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_A3_P1_Source_AAA_AAAGameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_A3_P1_Source_AAA_AAAGameMode_h_12_SPARSE_DATA \
	FID_A3_P1_Source_AAA_AAAGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_A3_P1_Source_AAA_AAAGameMode_h_12_INCLASS_NO_PURE_DECLS \
	FID_A3_P1_Source_AAA_AAAGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> AAA_API UClass* StaticClass<class AAAAGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_A3_P1_Source_AAA_AAAGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
