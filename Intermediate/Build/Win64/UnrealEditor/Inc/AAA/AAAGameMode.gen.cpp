// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AAA/AAAGameMode.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAAAGameMode() {}
// Cross Module References
	AAA_API UClass* Z_Construct_UClass_AAAAGameMode_NoRegister();
	AAA_API UClass* Z_Construct_UClass_AAAAGameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_AAA();
// End Cross Module References
	void AAAAGameMode::StaticRegisterNativesAAAAGameMode()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(AAAAGameMode);
	UClass* Z_Construct_UClass_AAAAGameMode_NoRegister()
	{
		return AAAAGameMode::StaticClass();
	}
	struct Z_Construct_UClass_AAAAGameMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AAAAGameMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_AAA,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AAAAGameMode_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering HLOD WorldPartition DataLayers Transformation" },
		{ "IncludePath", "AAAGameMode.h" },
		{ "ModuleRelativePath", "AAAGameMode.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AAAAGameMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AAAAGameMode>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_AAAAGameMode_Statics::ClassParams = {
		&AAAAGameMode::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008802ACu,
		METADATA_PARAMS(Z_Construct_UClass_AAAAGameMode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AAAAGameMode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AAAAGameMode()
	{
		if (!Z_Registration_Info_UClass_AAAAGameMode.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_AAAAGameMode.OuterSingleton, Z_Construct_UClass_AAAAGameMode_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_AAAAGameMode.OuterSingleton;
	}
	template<> AAA_API UClass* StaticClass<AAAAGameMode>()
	{
		return AAAAGameMode::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(AAAAGameMode);
	struct Z_CompiledInDeferFile_FID_A3_P1_Source_AAA_AAAGameMode_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_A3_P1_Source_AAA_AAAGameMode_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_AAAAGameMode, AAAAGameMode::StaticClass, TEXT("AAAAGameMode"), &Z_Registration_Info_UClass_AAAAGameMode, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(AAAAGameMode), 1648212571U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_A3_P1_Source_AAA_AAAGameMode_h_705928248(TEXT("/Script/AAA"),
		Z_CompiledInDeferFile_FID_A3_P1_Source_AAA_AAAGameMode_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_A3_P1_Source_AAA_AAAGameMode_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
