// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AAA/AAACharacter.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAAACharacter() {}
// Cross Module References
	AAA_API UClass* Z_Construct_UClass_AAAACharacter_NoRegister();
	AAA_API UClass* Z_Construct_UClass_AAAACharacter();
	ENGINE_API UClass* Z_Construct_UClass_ACharacter();
	UPackage* Z_Construct_UPackage__Script_AAA();
	ENGINE_API UClass* Z_Construct_UClass_USpringArmComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UCameraComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USkeletalMeshComponent_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USoundWave_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(AAAACharacter::execResetDamage)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ResetDamage();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AAAACharacter::execChangehealth)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_amount);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Changehealth(Z_Param_amount);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AAAACharacter::execshoot)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->shoot();
		P_NATIVE_END;
	}
	void AAAACharacter::StaticRegisterNativesAAAACharacter()
	{
		UClass* Class = AAAACharacter::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "Changehealth", &AAAACharacter::execChangehealth },
			{ "ResetDamage", &AAAACharacter::execResetDamage },
			{ "shoot", &AAAACharacter::execshoot },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AAAACharacter_Changehealth_Statics
	{
		struct AAACharacter_eventChangehealth_Parms
		{
			float amount;
		};
		static const UECodeGen_Private::FFloatPropertyParams NewProp_amount;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_AAAACharacter_Changehealth_Statics::NewProp_amount = { "amount", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AAACharacter_eventChangehealth_Parms, amount), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AAAACharacter_Changehealth_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AAAACharacter_Changehealth_Statics::NewProp_amount,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AAAACharacter_Changehealth_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "AAACharacter.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_AAAACharacter_Changehealth_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AAAACharacter, nullptr, "Changehealth", nullptr, nullptr, sizeof(Z_Construct_UFunction_AAAACharacter_Changehealth_Statics::AAACharacter_eventChangehealth_Parms), Z_Construct_UFunction_AAAACharacter_Changehealth_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AAAACharacter_Changehealth_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AAAACharacter_Changehealth_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AAAACharacter_Changehealth_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AAAACharacter_Changehealth()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_AAAACharacter_Changehealth_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AAAACharacter_ResetDamage_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AAAACharacter_ResetDamage_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "AAACharacter.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_AAAACharacter_ResetDamage_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AAAACharacter, nullptr, "ResetDamage", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AAAACharacter_ResetDamage_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AAAACharacter_ResetDamage_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AAAACharacter_ResetDamage()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_AAAACharacter_ResetDamage_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AAAACharacter_shoot_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AAAACharacter_shoot_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "AAACharacter.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_AAAACharacter_shoot_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AAAACharacter, nullptr, "shoot", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AAAACharacter_shoot_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AAAACharacter_shoot_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AAAACharacter_shoot()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_AAAACharacter_shoot_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(AAAACharacter);
	UClass* Z_Construct_UClass_AAAACharacter_NoRegister()
	{
		return AAAACharacter::StaticClass();
	}
	struct Z_Construct_UClass_AAAACharacter_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_CameraBoom_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_CameraBoom;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_FollowCamera_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_FollowCamera;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_FirePoint_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_FirePoint;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_TurnRateGamepad_MetaData[];
#endif
		static const UECodeGen_Private::FFloatPropertyParams NewProp_TurnRateGamepad;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_SK_FPGun_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_SK_FPGun;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_CharacterRotationSpeed_MetaData[];
#endif
		static const UECodeGen_Private::FFloatPropertyParams NewProp_CharacterRotationSpeed;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_MaxHealth_MetaData[];
#endif
		static const UECodeGen_Private::FFloatPropertyParams NewProp_MaxHealth;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Health_MetaData[];
#endif
		static const UECodeGen_Private::FFloatPropertyParams NewProp_Health;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Bullet_MetaData[];
#endif
		static const UECodeGen_Private::FClassPropertyParams NewProp_Bullet;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_sound_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_sound;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_HealthAlteration_MetaData[];
#endif
		static const UECodeGen_Private::FFloatPropertyParams NewProp_HealthAlteration;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_TakeDamage_MetaData[];
#endif
		static void NewProp_TakeDamage_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_TakeDamage;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AAAACharacter_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ACharacter,
		(UObject* (*)())Z_Construct_UPackage__Script_AAA,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AAAACharacter_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AAAACharacter_Changehealth, "Changehealth" }, // 2024233169
		{ &Z_Construct_UFunction_AAAACharacter_ResetDamage, "ResetDamage" }, // 4059578366
		{ &Z_Construct_UFunction_AAAACharacter_shoot, "shoot" }, // 569031992
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AAAACharacter_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Navigation" },
		{ "IncludePath", "AAACharacter.h" },
		{ "ModuleRelativePath", "AAACharacter.h" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AAAACharacter_Statics::NewProp_CameraBoom_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Camera" },
		{ "Comment", "/** Camera boom positioning the camera behind the character */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "AAACharacter.h" },
		{ "ToolTip", "Camera boom positioning the camera behind the character" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AAAACharacter_Statics::NewProp_CameraBoom = { "CameraBoom", nullptr, (EPropertyFlags)0x00400000000a001d, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AAAACharacter, CameraBoom), Z_Construct_UClass_USpringArmComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AAAACharacter_Statics::NewProp_CameraBoom_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AAAACharacter_Statics::NewProp_CameraBoom_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AAAACharacter_Statics::NewProp_FollowCamera_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Camera" },
		{ "Comment", "/** Follow camera */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "AAACharacter.h" },
		{ "ToolTip", "Follow camera" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AAAACharacter_Statics::NewProp_FollowCamera = { "FollowCamera", nullptr, (EPropertyFlags)0x004000000008001d, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AAAACharacter, FollowCamera), Z_Construct_UClass_UCameraComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AAAACharacter_Statics::NewProp_FollowCamera_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AAAACharacter_Statics::NewProp_FollowCamera_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AAAACharacter_Statics::NewProp_FirePoint_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Camera" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "AAACharacter.h" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AAAACharacter_Statics::NewProp_FirePoint = { "FirePoint", nullptr, (EPropertyFlags)0x004000000008001d, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AAAACharacter, FirePoint), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AAAACharacter_Statics::NewProp_FirePoint_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AAAACharacter_Statics::NewProp_FirePoint_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AAAACharacter_Statics::NewProp_TurnRateGamepad_MetaData[] = {
		{ "Category", "Input" },
		{ "Comment", "/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */" },
		{ "ModuleRelativePath", "AAACharacter.h" },
		{ "ToolTip", "Base turn rate, in deg/sec. Other scaling may affect final turn rate." },
	};
#endif
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AAAACharacter_Statics::NewProp_TurnRateGamepad = { "TurnRateGamepad", nullptr, (EPropertyFlags)0x0010000000020015, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AAAACharacter, TurnRateGamepad), METADATA_PARAMS(Z_Construct_UClass_AAAACharacter_Statics::NewProp_TurnRateGamepad_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AAAACharacter_Statics::NewProp_TurnRateGamepad_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AAAACharacter_Statics::NewProp_SK_FPGun_MetaData[] = {
		{ "Category", "AAACharacter" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "AAACharacter.h" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AAAACharacter_Statics::NewProp_SK_FPGun = { "SK_FPGun", nullptr, (EPropertyFlags)0x001000000008000d, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AAAACharacter, SK_FPGun), Z_Construct_UClass_USkeletalMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AAAACharacter_Statics::NewProp_SK_FPGun_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AAAACharacter_Statics::NewProp_SK_FPGun_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AAAACharacter_Statics::NewProp_CharacterRotationSpeed_MetaData[] = {
		{ "Category", "AAACharacter" },
		{ "ModuleRelativePath", "AAACharacter.h" },
	};
#endif
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AAAACharacter_Statics::NewProp_CharacterRotationSpeed = { "CharacterRotationSpeed", nullptr, (EPropertyFlags)0x0020080000000001, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AAAACharacter, CharacterRotationSpeed), METADATA_PARAMS(Z_Construct_UClass_AAAACharacter_Statics::NewProp_CharacterRotationSpeed_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AAAACharacter_Statics::NewProp_CharacterRotationSpeed_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AAAACharacter_Statics::NewProp_MaxHealth_MetaData[] = {
		{ "Category", "Data" },
		{ "Comment", "//MaxHp\n" },
		{ "ModuleRelativePath", "AAACharacter.h" },
		{ "ToolTip", "MaxHp" },
	};
#endif
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AAAACharacter_Statics::NewProp_MaxHealth = { "MaxHealth", nullptr, (EPropertyFlags)0x0010000000000005, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AAAACharacter, MaxHealth), METADATA_PARAMS(Z_Construct_UClass_AAAACharacter_Statics::NewProp_MaxHealth_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AAAACharacter_Statics::NewProp_MaxHealth_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AAAACharacter_Statics::NewProp_Health_MetaData[] = {
		{ "Category", "Data" },
		{ "Comment", "//HP\n" },
		{ "ModuleRelativePath", "AAACharacter.h" },
		{ "ToolTip", "HP" },
	};
#endif
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AAAACharacter_Statics::NewProp_Health = { "Health", nullptr, (EPropertyFlags)0x0010000000000005, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AAAACharacter, Health), METADATA_PARAMS(Z_Construct_UClass_AAAACharacter_Statics::NewProp_Health_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AAAACharacter_Statics::NewProp_Health_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AAAACharacter_Statics::NewProp_Bullet_MetaData[] = {
		{ "Category", "Data" },
		{ "ModuleRelativePath", "AAACharacter.h" },
	};
#endif
	const UECodeGen_Private::FClassPropertyParams Z_Construct_UClass_AAAACharacter_Statics::NewProp_Bullet = { "Bullet", nullptr, (EPropertyFlags)0x0014000000000005, UECodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AAAACharacter, Bullet), Z_Construct_UClass_AActor_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_AAAACharacter_Statics::NewProp_Bullet_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AAAACharacter_Statics::NewProp_Bullet_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AAAACharacter_Statics::NewProp_sound_MetaData[] = {
		{ "Category", "Data" },
		{ "ModuleRelativePath", "AAACharacter.h" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AAAACharacter_Statics::NewProp_sound = { "sound", nullptr, (EPropertyFlags)0x0010000000000005, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AAAACharacter, sound), Z_Construct_UClass_USoundWave_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AAAACharacter_Statics::NewProp_sound_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AAAACharacter_Statics::NewProp_sound_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AAAACharacter_Statics::NewProp_HealthAlteration_MetaData[] = {
		{ "Category", "AAACharacter" },
		{ "ModuleRelativePath", "AAACharacter.h" },
	};
#endif
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AAAACharacter_Statics::NewProp_HealthAlteration = { "HealthAlteration", nullptr, (EPropertyFlags)0x0010000000000001, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AAAACharacter, HealthAlteration), METADATA_PARAMS(Z_Construct_UClass_AAAACharacter_Statics::NewProp_HealthAlteration_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AAAACharacter_Statics::NewProp_HealthAlteration_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AAAACharacter_Statics::NewProp_TakeDamage_MetaData[] = {
		{ "Category", "AAACharacter" },
		{ "ModuleRelativePath", "AAACharacter.h" },
	};
#endif
	void Z_Construct_UClass_AAAACharacter_Statics::NewProp_TakeDamage_SetBit(void* Obj)
	{
		((AAAACharacter*)Obj)->TakeDamage = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AAAACharacter_Statics::NewProp_TakeDamage = { "TakeDamage", nullptr, (EPropertyFlags)0x0010000000000001, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AAAACharacter), &Z_Construct_UClass_AAAACharacter_Statics::NewProp_TakeDamage_SetBit, METADATA_PARAMS(Z_Construct_UClass_AAAACharacter_Statics::NewProp_TakeDamage_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AAAACharacter_Statics::NewProp_TakeDamage_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AAAACharacter_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AAAACharacter_Statics::NewProp_CameraBoom,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AAAACharacter_Statics::NewProp_FollowCamera,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AAAACharacter_Statics::NewProp_FirePoint,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AAAACharacter_Statics::NewProp_TurnRateGamepad,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AAAACharacter_Statics::NewProp_SK_FPGun,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AAAACharacter_Statics::NewProp_CharacterRotationSpeed,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AAAACharacter_Statics::NewProp_MaxHealth,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AAAACharacter_Statics::NewProp_Health,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AAAACharacter_Statics::NewProp_Bullet,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AAAACharacter_Statics::NewProp_sound,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AAAACharacter_Statics::NewProp_HealthAlteration,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AAAACharacter_Statics::NewProp_TakeDamage,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AAAACharacter_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AAAACharacter>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_AAAACharacter_Statics::ClassParams = {
		&AAAACharacter::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_AAAACharacter_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_AAAACharacter_Statics::PropPointers),
		0,
		0x008000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AAAACharacter_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AAAACharacter_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AAAACharacter()
	{
		if (!Z_Registration_Info_UClass_AAAACharacter.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_AAAACharacter.OuterSingleton, Z_Construct_UClass_AAAACharacter_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_AAAACharacter.OuterSingleton;
	}
	template<> AAA_API UClass* StaticClass<AAAACharacter>()
	{
		return AAAACharacter::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(AAAACharacter);
	struct Z_CompiledInDeferFile_FID_A3_P1_Source_AAA_AAACharacter_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_A3_P1_Source_AAA_AAACharacter_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_AAAACharacter, AAAACharacter::StaticClass, TEXT("AAAACharacter"), &Z_Registration_Info_UClass_AAAACharacter, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(AAAACharacter), 3552505916U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_A3_P1_Source_AAA_AAACharacter_h_1877709711(TEXT("/Script/AAA"),
		Z_CompiledInDeferFile_FID_A3_P1_Source_AAA_AAACharacter_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_A3_P1_Source_AAA_AAACharacter_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
