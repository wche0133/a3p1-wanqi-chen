// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef AAA_HealthBarWidget_generated_h
#error "HealthBarWidget.generated.h already included, missing '#pragma once' in HealthBarWidget.h"
#endif
#define AAA_HealthBarWidget_generated_h

#define FID_A3_P1_Source_AAA_Public_HealthBarWidget_h_17_SPARSE_DATA
#define FID_A3_P1_Source_AAA_Public_HealthBarWidget_h_17_RPC_WRAPPERS
#define FID_A3_P1_Source_AAA_Public_HealthBarWidget_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_A3_P1_Source_AAA_Public_HealthBarWidget_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUHealthBarWidget(); \
	friend struct Z_Construct_UClass_UHealthBarWidget_Statics; \
public: \
	DECLARE_CLASS(UHealthBarWidget, UUserWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AAA"), NO_API) \
	DECLARE_SERIALIZER(UHealthBarWidget)


#define FID_A3_P1_Source_AAA_Public_HealthBarWidget_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUHealthBarWidget(); \
	friend struct Z_Construct_UClass_UHealthBarWidget_Statics; \
public: \
	DECLARE_CLASS(UHealthBarWidget, UUserWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AAA"), NO_API) \
	DECLARE_SERIALIZER(UHealthBarWidget)


#define FID_A3_P1_Source_AAA_Public_HealthBarWidget_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UHealthBarWidget(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UHealthBarWidget) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UHealthBarWidget); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UHealthBarWidget); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UHealthBarWidget(UHealthBarWidget&&); \
	NO_API UHealthBarWidget(const UHealthBarWidget&); \
public:


#define FID_A3_P1_Source_AAA_Public_HealthBarWidget_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UHealthBarWidget(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UHealthBarWidget(UHealthBarWidget&&); \
	NO_API UHealthBarWidget(const UHealthBarWidget&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UHealthBarWidget); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UHealthBarWidget); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UHealthBarWidget)


#define FID_A3_P1_Source_AAA_Public_HealthBarWidget_h_14_PROLOG
#define FID_A3_P1_Source_AAA_Public_HealthBarWidget_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_A3_P1_Source_AAA_Public_HealthBarWidget_h_17_SPARSE_DATA \
	FID_A3_P1_Source_AAA_Public_HealthBarWidget_h_17_RPC_WRAPPERS \
	FID_A3_P1_Source_AAA_Public_HealthBarWidget_h_17_INCLASS \
	FID_A3_P1_Source_AAA_Public_HealthBarWidget_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_A3_P1_Source_AAA_Public_HealthBarWidget_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_A3_P1_Source_AAA_Public_HealthBarWidget_h_17_SPARSE_DATA \
	FID_A3_P1_Source_AAA_Public_HealthBarWidget_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_A3_P1_Source_AAA_Public_HealthBarWidget_h_17_INCLASS_NO_PURE_DECLS \
	FID_A3_P1_Source_AAA_Public_HealthBarWidget_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> AAA_API UClass* StaticClass<class UHealthBarWidget>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_A3_P1_Source_AAA_Public_HealthBarWidget_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
