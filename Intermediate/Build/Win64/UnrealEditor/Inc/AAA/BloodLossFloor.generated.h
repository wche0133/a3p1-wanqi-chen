// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FHitResult;
#ifdef AAA_BloodLossFloor_generated_h
#error "BloodLossFloor.generated.h already included, missing '#pragma once' in BloodLossFloor.h"
#endif
#define AAA_BloodLossFloor_generated_h

#define FID_A3_P1_Source_AAA_Public_BloodLossFloor_h_12_SPARSE_DATA
#define FID_A3_P1_Source_AAA_Public_BloodLossFloor_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnHitboxOverlapEnd); \
	DECLARE_FUNCTION(execOnHitboxOverlapBegin);


#define FID_A3_P1_Source_AAA_Public_BloodLossFloor_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnHitboxOverlapEnd); \
	DECLARE_FUNCTION(execOnHitboxOverlapBegin);


#define FID_A3_P1_Source_AAA_Public_BloodLossFloor_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABloodLossFloor(); \
	friend struct Z_Construct_UClass_ABloodLossFloor_Statics; \
public: \
	DECLARE_CLASS(ABloodLossFloor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/AAA"), NO_API) \
	DECLARE_SERIALIZER(ABloodLossFloor)


#define FID_A3_P1_Source_AAA_Public_BloodLossFloor_h_12_INCLASS \
private: \
	static void StaticRegisterNativesABloodLossFloor(); \
	friend struct Z_Construct_UClass_ABloodLossFloor_Statics; \
public: \
	DECLARE_CLASS(ABloodLossFloor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/AAA"), NO_API) \
	DECLARE_SERIALIZER(ABloodLossFloor)


#define FID_A3_P1_Source_AAA_Public_BloodLossFloor_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABloodLossFloor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABloodLossFloor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABloodLossFloor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABloodLossFloor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABloodLossFloor(ABloodLossFloor&&); \
	NO_API ABloodLossFloor(const ABloodLossFloor&); \
public:


#define FID_A3_P1_Source_AAA_Public_BloodLossFloor_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABloodLossFloor(ABloodLossFloor&&); \
	NO_API ABloodLossFloor(const ABloodLossFloor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABloodLossFloor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABloodLossFloor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ABloodLossFloor)


#define FID_A3_P1_Source_AAA_Public_BloodLossFloor_h_9_PROLOG
#define FID_A3_P1_Source_AAA_Public_BloodLossFloor_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_A3_P1_Source_AAA_Public_BloodLossFloor_h_12_SPARSE_DATA \
	FID_A3_P1_Source_AAA_Public_BloodLossFloor_h_12_RPC_WRAPPERS \
	FID_A3_P1_Source_AAA_Public_BloodLossFloor_h_12_INCLASS \
	FID_A3_P1_Source_AAA_Public_BloodLossFloor_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_A3_P1_Source_AAA_Public_BloodLossFloor_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_A3_P1_Source_AAA_Public_BloodLossFloor_h_12_SPARSE_DATA \
	FID_A3_P1_Source_AAA_Public_BloodLossFloor_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_A3_P1_Source_AAA_Public_BloodLossFloor_h_12_INCLASS_NO_PURE_DECLS \
	FID_A3_P1_Source_AAA_Public_BloodLossFloor_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> AAA_API UClass* StaticClass<class ABloodLossFloor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_A3_P1_Source_AAA_Public_BloodLossFloor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
