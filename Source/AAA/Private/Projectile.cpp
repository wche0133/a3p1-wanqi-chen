// Fill out your copyright notice in the Description page of Project Settings.


#include "Projectile.h"

// Sets default values
AProjectile::AProjectile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	RootComponent = SceneComponent;

	BulletMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Bullet Mesh"));
	BulletMesh->OnComponentBeginOverlap.AddDynamic(this, &AProjectile::OnBulletHit);
	BulletMesh->SetupAttachment(RootComponent);

	Parent = nullptr;
	MovementSpeed = 950;
	MaximumLifetime = 5;
	CurrentLifetime = 0;

}

// Called when the game starts or when spawned
void AProjectile::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FVector CurrentLocation = GetActorLocation();
	//FVector Forward = FVector(1, 0, 0);
	FVector Forward = GetActorForwardVector();
	FVector Movement = (Forward * MovementSpeed * DeltaTime);
	FVector NewLocation = CurrentLocation + Movement;
	SetActorLocation(NewLocation);

	CurrentLifetime += DeltaTime;
	if (CurrentLifetime >= MaximumLifetime) {
		Destroy();
	}

}

void AProjectile::OnBulletHit(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) {
	if (OtherActor && OtherActor != this) {
		if (!Parent)
			Destroy();
			//return;
		if (Parent == OtherActor)
			return;
	}

		Destroy();

}

