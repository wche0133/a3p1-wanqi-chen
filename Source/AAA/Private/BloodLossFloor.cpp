// Fill out your copyright notice in the Description page of Project Settings.


#include "BloodLossFloor.h"
#include "../AAACharacter.h"
#include "Components/BoxComponent.h"

// Sets default values
ABloodLossFloor::ABloodLossFloor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Every Actor contains a root component. We initialize this as a scene component
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root Component"));

	// We create a mesh that will be our visual representation of the object and attach it to the root
	BloodLossFloorMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh Component"));
	BloodLossFloorMesh->SetupAttachment(RootComponent);

	// We create a hitbox that will be our collider that we can walk over to activate. This is also attached to root
	BloodLossFloorBox = CreateDefaultSubobject<UBoxComponent>(TEXT("Hitbox Component"));
	BloodLossFloorBox->SetupAttachment(RootComponent);

}

// Called when the game starts or when spawned
void ABloodLossFloor::BeginPlay()
{
	Super::BeginPlay();
	
	BloodLossFloorBox->OnComponentBeginOverlap.AddDynamic(this, &ABloodLossFloor::OnHitboxOverlapBegin);
	BloodLossFloorBox->OnComponentEndOverlap.AddDynamic(this, &ABloodLossFloor::OnHitboxOverlapEnd);

}

// Called every frame
void ABloodLossFloor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABloodLossFloor::OnHitboxOverlapBegin(UPrimitiveComponent* OverlappedComponent,
	AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex,
	bool bFromSweep, const FHitResult& SweepResult) {
	if (OtherActor && OtherActor != this && Cast<AAAACharacter>(OtherActor))
	{
		Cast<AAAACharacter>(OtherActor)->Changehealth(HealthAlteration);
		if (Cast<AAAACharacter>(OtherActor) -> Health <=0.0f) {
			OtherActor->Destroy();
		}
	}
}

void ABloodLossFloor::OnHitboxOverlapEnd(UPrimitiveComponent* OverlappedComponent,
	AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex) {
	if (OtherActor && OtherActor != this && Cast<AAAACharacter>(OtherActor))
	{
		Cast<AAAACharacter>(OtherActor)->ResetDamage();
	}
}



