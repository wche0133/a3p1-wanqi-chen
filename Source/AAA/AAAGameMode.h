// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "AAAGameMode.generated.h"

UCLASS(minimalapi)
class AAAAGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AAAAGameMode();
};



