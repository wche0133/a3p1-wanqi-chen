// Copyright Epic Games, Inc. All Rights Reserved.

#include "AAAGameMode.h"
#include "AAACharacter.h"
#include "UObject/ConstructorHelpers.h"

AAAAGameMode::AAAAGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPerson/Blueprints/BP_ThirdPersonCharacter"));

	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
